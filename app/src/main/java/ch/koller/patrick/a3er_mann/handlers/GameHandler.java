package ch.koller.patrick.a3er_mann.handlers;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import ch.koller.patrick.a3er_mann.R;
import ch.koller.patrick.a3er_mann.types.DataProvider;
import ch.koller.patrick.a3er_mann.types.GameRule;
import ch.koller.patrick.a3er_mann.types.IndivRule;
import ch.koller.patrick.a3er_mann.types.PairRule;
import ch.koller.patrick.a3er_mann.types.Player;

/**
 * Created by admin on 20.09.2017.
 */
public class GameHandler {
    private DataProvider dataProvider;
    private List<PairRule> pairRules;
    private List<IndivRule> indivRules;
    private Player activePlayer;
    private Player threeMan;
    private IndivRule customRule;
    private int diceEyes1;
    private int diceEyes2;
    private Context context;
    private boolean newTaskToChoose = false;
    private int icon_size;

    public GameHandler(DataProvider dataProvider, Context context) {
        this.dataProvider = dataProvider;
        this.context = context;

        this.pairRules = new ArrayList<>();
        this.indivRules = new ArrayList<>();

        icon_size = 50 * ((int) context.getResources().getDisplayMetrics().density);

        pairRules.add(new PairRule(context.getResources().getString(R.string.newTask), 12));
        pairRules.add(new PairRule(context.getResources().getString(R.string.taskAll), 10));
        pairRules.add(new PairRule(context.getResources().getString(R.string.taskAll), 8));
        pairRules.add(new PairRule(context.getResources().getString(R.string.taskAll), 6));
        pairRules.add(new PairRule(context.getResources().getString(R.string.taskAll) + " " + context.getResources().getString(R.string.taskThreeManTwice), 4));
        pairRules.add(new PairRule(context.getResources().getString(R.string.taskAll) + " " + context.getResources().getString(R.string.taskThreeManTwice), 2));

        indivRules.add(new IndivRule(context.getResources().getString(R.string.taskLeft), 5));
        indivRules.add(new IndivRule(context.getResources().getString(R.string.taskRight), 7));
    }

    public String rollDices(Player activePlayer, String currentTask, ImageView currentThreeManImg, ImageView executorImageView, ImageView executorImageView2, TextView executorTxv, LinearLayout taskComponentsHolder) {
        String output = "";
        GameRule rule;
        boolean nobodyExecutesTask = false;
        this.activePlayer = activePlayer;
        newTaskToChoose = false;

        if (diceEyes1 == 1 && diceEyes2 == 2 || diceEyes1 == 2 && diceEyes2 == 1) {
            output = context.getResources().getString(R.string.threeMan);

            for (Player player : dataProvider.getPlayers()) {
                if (player.equals(activePlayer)) {
                    player.setScore(player.getScore() + 1);
                }
            }
            Picasso.with(context)
                    .load(activePlayer.getPlayerIcon())
                    .resize(icon_size, icon_size)
                    .centerCrop()
                    .into(currentThreeManImg);
            //currentThreeManImg.setImageResource(activePlayer.getPlayerIcon());
            return output;
        } else if (isPair()) {
            rule = checkForPairRules();
            if (rule != null) {
                output += needSpace(output) + checkForPairRules().getTask();
                if (output.contains(context.getResources().getString(R.string.newTask))) {
                    newTaskToChoose = true;
                    taskComponentsHolder.setVisibility(View.VISIBLE);
                } else {
                    Picasso.with(context)
                            .load(R.drawable.zoo)
                            .resize(icon_size, icon_size)
                            .centerCrop()
                            .into(executorImageView);
                    //executorImageView.setImageResource(R.drawable.zoo);
                }
            } else {
                output += "";
            }
        } else if (output.equals("")) {
            rule = checkForIndivRules();
            if (rule != null) {
                output += needSpace(output) + checkForIndivRules().getTask();

                if (output.contains(context.getResources().getString(R.string.taskRight))) {
                    Picasso.with(context)
                            .load(getNextPlayer(activePlayer).getPlayerIcon())
                            .resize(icon_size, icon_size)
                            .centerCrop()
                            .into(executorImageView);
                    //executorImageView.setImageResource(getNextPlayer(activePlayer).getPlayerIcon());
                } else if (output.contains(context.getResources().getString(R.string.taskLeft))) {
                    Picasso.with(context)
                            .load(getPreviousPlayer(activePlayer).getPlayerIcon())
                            .resize(icon_size, icon_size)
                            .centerCrop()
                            .into(executorImageView);
                    //executorImageView.setImageResource(getPreviousPlayer(activePlayer).getPlayerIcon());
                } else {
                    Picasso.with(context)
                            .load(activePlayer.getPlayerIcon())
                            .resize(icon_size, icon_size)
                            .centerCrop()
                            .into(executorImageView);
                    //executorImageView.setImageResource(activePlayer.getPlayerIcon());
                }
            } else {
                output += "";
            }
        }
        if (isThreeManAction()) {
            Picasso.with(context)
                    .load(threeMan.getPlayerIcon())
                    .resize(icon_size, icon_size)
                    .centerCrop()
                    .into(executorImageView2);
            //executorImageView2.setImageResource(threeMan.getPlayerIcon());
            if(!output.contains(context.getResources().getString(R.string.taskThreeManTwice))){
                output += needSpace(output) + context.getResources().getString(R.string.taskThreeManOnce);
            }
        } else if (output.equals("")) {
            output = context.getResources().getString(R.string.taskNobody);
            nobodyExecutesTask = true;
        }

        if (!nobodyExecutesTask) {
            executorTxv.setText(currentTask);
        }

        return output;
    }

    private Player getNextPlayer(Player currentPlayer) {
        int index = 0;
        for (Player player : getPlayers()) {
            if (player.equals(currentPlayer)) {
                break;
            }
            index++;
        }

        index++;
        if (index >= getPlayers().size()) {
            index = 0;
        }

        return getPlayers().get(index);
    }

    private Player getPreviousPlayer(Player currentPlayer) {
        int index = 0;
        for (Player player : getPlayers()) {
            if (player.equals(currentPlayer)) {
                break;
            }
            index++;
        }

        index--;
        if (index <= 0) {
            index = getPlayers().size() - 1;
        }

        return getPlayers().get(index);
    }

    public boolean isNewTaskToChoose() {
        return newTaskToChoose;
    }

    private String needSpace(String output) {
        if (!output.equals("")) {
            return " ";
        }
        return "";
    }

    private boolean isPair() {
        return diceEyes1 == diceEyes2;
    }

    private boolean isThreeManAction() {
        return diceEyes1 == 1 || diceEyes1 == 2 || diceEyes2 == 1 || diceEyes2 == 2;
    }

    private PairRule checkForPairRules() {
        for (PairRule pairRule : pairRules) {
            if ((diceEyes1 + diceEyes2) == pairRule.getEyeCount()) {
                return pairRule;
            }
        }
        return null;
    }

    private IndivRule checkForIndivRules() {
        for (IndivRule indivRule : indivRules) {
            if ((diceEyes1 + diceEyes2) == indivRule.getEyeCount()) {
                return indivRule;
            }
        }
        return null;
    }

    public int getDiceEyes2() {
        return diceEyes2;
    }

    public void setDiceEyes2(int diceEyes2) {
        this.diceEyes2 = diceEyes2;
    }

    public LinkedList<Player> getPlayers() {
        return dataProvider.getPlayers();
    }

    public List<PairRule> getPairRules() {
        return pairRules;
    }

    public void setPairRules(List<PairRule> pairRules) {
        this.pairRules = pairRules;
    }

    public List<IndivRule> getIndivRules() {
        return indivRules;
    }

    public void setIndivRules(List<IndivRule> indivRules) {
        this.indivRules = indivRules;
    }

    public Player getActivePlayer() {
        return activePlayer;
    }

    public void setActivePlayer(Player activePlayer) {
        this.activePlayer = activePlayer;
    }

    public Player getThreeMan() {
        return threeMan;
    }

    public void setThreeMan(Player threeMan) {
        this.threeMan = threeMan;
    }

    public IndivRule getCustomRule() {
        return customRule;
    }

    public void setCustomRule(IndivRule customRule) {
        this.customRule = customRule;
    }

    public int getDiceEyes1() {
        return diceEyes1;
    }

    public void setDiceEyes1(int diceEyes1) {
        this.diceEyes1 = diceEyes1;
    }
}