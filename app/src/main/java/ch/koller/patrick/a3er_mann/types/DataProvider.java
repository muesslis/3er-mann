package ch.koller.patrick.a3er_mann.types;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by admin on 20.09.2017.
 */
public class DataProvider implements Serializable {
    private LinkedList<Player> players;
    private String task;
    private String newChosenTask;
    private List<String> tasks;

    public DataProvider(){
        players = new LinkedList<>();
        task = "";
        newChosenTask = null;
    }

    public LinkedList<Player> getPlayers() {
        return players;
    }

    public void addPlayer(Player player){
        players.add(player);
    }

    public String getTask(){
        return task;
    }

    public void setTask(String task){
        this.task = task;
    }

    public void setNewChosenTask(String newChosenTask){
        this.newChosenTask = newChosenTask;
    }

    public String getNewChosenTask(){
        String temp = newChosenTask;
        newChosenTask = null;
        if(temp == null){
            return task;
        }
        return temp;
    }

    public void setPlayers(LinkedList<Player> players) {
        this.players = players;
    }

    public List<String> getTasks() {
        return tasks;
    }

    public void setTasks(List<String> tasks) {
        this.tasks = tasks;
    }
}
