package ch.koller.patrick.a3er_mann.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import ch.koller.patrick.a3er_mann.types.DataProvider;
import ch.koller.patrick.a3er_mann.MainActivity;
import ch.koller.patrick.a3er_mann.R;
import ch.koller.patrick.a3er_mann.adapters.TaskAdapterForActivity;
import ch.koller.patrick.a3er_mann.adapters.TaskAdapterForFragment;

/**
 * Created by admin on 25.09.2017.
 */
public class TaskChooserFragment extends Fragment {

    private ListView taskHolder;
    private DataProvider dataProvider;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_chooser, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        initComponents();
    }

    private void initComponents() {

        Bundle bundle = getArguments();
        dataProvider = (DataProvider) bundle.getSerializable("data");

        taskHolder = ((ListView) getActivity().findViewById(R.id.taskHolder));

        BaseAdapter baseAdapter;

        if(getActivity() instanceof MainActivity){
            baseAdapter = new TaskAdapterForFragment(dataProvider.getTasks(), dataProvider, getActivity());
        } else {
            hideEverything();
            baseAdapter = new TaskAdapterForActivity(dataProvider.getTasks(), dataProvider, getActivity());
        }

        taskHolder.setAdapter(baseAdapter);
    }

    private void hideEverything(){
        TextView threeManTxv = (TextView) getActivity().findViewById(R.id.threeManTxv);
        ImageView threeManImg = (ImageView) getActivity().findViewById(R.id.threeManImg);
        TextView activePlayerTxv = (TextView) getActivity().findViewById(R.id.activePlayerTxv);
        ImageView activePlayerImg = (ImageView) getActivity().findViewById(R.id.activePlayerImg);
        ImageView dice1 = (ImageView) getActivity().findViewById(R.id.dice1);
        ImageView dice2 = (ImageView) getActivity().findViewById(R.id.dice2);
        TextView taskTxv = (TextView) getActivity().findViewById(R.id.taskTxv);
        TextView executorTxv = (TextView) getActivity().findViewById(R.id.executorTxv);
        LinearLayout taskComponentsHolder = (LinearLayout)getActivity().findViewById(R.id.task_components_holder);
        ImageView imageView = (ImageView) getActivity().findViewById(R.id.imageView);
        ImageView executorPlayerImg2 = (ImageView) getActivity().findViewById(R.id.executorPlayerImg2);
        ImageView executorPlayerImg = (ImageView) getActivity().findViewById(R.id.executorPlayerImg);

        threeManTxv.setVisibility(View.INVISIBLE);
        threeManImg.setVisibility(View.INVISIBLE);
        activePlayerTxv.setVisibility(View.INVISIBLE);
        activePlayerImg.setVisibility(View.INVISIBLE);
        dice1.setVisibility(View.INVISIBLE);
        dice2.setVisibility(View.INVISIBLE);
        taskTxv.setVisibility(View.INVISIBLE);
        executorTxv.setVisibility(View.INVISIBLE);
        taskComponentsHolder.setVisibility(View.INVISIBLE);
        imageView.setVisibility(View.INVISIBLE);
        executorPlayerImg.setVisibility(View.INVISIBLE);
        executorPlayerImg2.setVisibility(View.INVISIBLE);
    }
}
