package ch.koller.patrick.a3er_mann;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Random;

import ch.koller.patrick.a3er_mann.fragments.StatisticFragment;
import ch.koller.patrick.a3er_mann.fragments.TaskChooserFragment;
import ch.koller.patrick.a3er_mann.handlers.DatabaseHandler;
import ch.koller.patrick.a3er_mann.handlers.GameHandler;
import ch.koller.patrick.a3er_mann.handlers.ShakeEventManager;
import ch.koller.patrick.a3er_mann.types.DataProvider;
import ch.koller.patrick.a3er_mann.types.Player;

/**
 * Created by admin on 20.09.2017.
 */
public class GameActivity extends AppCompatActivity implements ShakeEventManager.OnShakeListener {
    private ShakeEventManager sd;
    private TextView threeManTxv;
    private ImageView threeManImg;
    private TextView activePlayerTxv;
    private ImageView activePlayerImg;
    private ImageView dice1;
    private ImageView dice2;
    private ImageView dice1Picture;
    private ImageView dice2Picture;
    private ImageView currentPlayerImg;
    private ImageView currentThreeManImg;
    private ImageView executorPlayerImg;
    private ImageView executorPlayerImg2;
    private LinearLayout taskComponentsHolder;
    private EditText taskInput;
    private Button saveTaskBtn;
    private ImageView imageView;
    private EditText taskTxv;
    private TextView taskView;
    private TextView executorTxv;
    private Button chooseTaskBtn;
    private Player currentPlayer;
    private Player currentThreeMan;
    private Random random = new Random();
    private GameHandler gameHandler;
    private final int MAXROUNDS = 10;
    private int currentPlayerIndex;
    private int shuffleCounter = 0;
    private String currentTask;
    private FragmentManager fragmentManager;
    private StatisticFragment statisticFragment;
    private DataProvider dataProvider;
    private int icon_size;
    private LinearLayout gameFrameHolder;
    private Menu menu;
    private DatabaseHandler databaseHandler;
    private boolean shakeForOPtionMenuStop = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        dataProvider = (DataProvider) getIntent().getExtras().get("data");
        currentTask = dataProvider.getTask();

        icon_size = getResources().getInteger(R.integer.icon_size_small) * ((int) getResources().getDisplayMetrics().density);

        dice1Picture = (ImageView) findViewById(R.id.dice1);
        dice2Picture = (ImageView) findViewById(R.id.dice2);

        gameHandler = new GameHandler(dataProvider, this.getApplicationContext());
        currentPlayerIndex = initComponents();

        ShakeEventManager.create(this, this);
    }

    private void showNextPlayer(int currentPlayerIndex) {
        currentPlayer = gameHandler.getPlayers().get(currentPlayerIndex);

        if (currentPlayer != null) {
            Picasso.with(getApplicationContext())
                    .load(currentPlayer.getPlayerIcon())
                    .resize(icon_size, icon_size)
                    .centerCrop()
                    .into(currentPlayerImg);
            executorPlayerImg.setImageResource(0);
            executorPlayerImg2.setImageResource(0);
            taskView.setText("");
            executorTxv.setText("");
            taskComponentsHolder.setVisibility(View.INVISIBLE);
            Toast.makeText(this, getApplicationContext().getResources().getString(R.string.nextPlayer), Toast.LENGTH_SHORT).show();
        }
    }

    //Clean up
    protected void onPause() {
        super.onPause();
        ShakeEventManager.stop();
    }

    protected void onDestroy() {
        super.onDestroy();
        ShakeEventManager.destroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        ShakeEventManager.start();
    }

    private int initComponents() {

        databaseHandler = new DatabaseHandler(this);

        threeManTxv = (TextView) findViewById(R.id.threeManTxv);
        threeManImg = (ImageView) findViewById(R.id.threeManImg);
        activePlayerTxv = (TextView) findViewById(R.id.activePlayerTxv);
        activePlayerImg = (ImageView) findViewById(R.id.activePlayerImg);
        dice1 = (ImageView) findViewById(R.id.dice1);
        dice2 = (ImageView) findViewById(R.id.dice2);
        currentPlayerImg = (ImageView) findViewById(R.id.activePlayerImg);
        currentThreeManImg = (ImageView) findViewById(R.id.threeManImg);
        taskInput = (EditText) findViewById(R.id.taskInput);
        imageView = (ImageView) findViewById(R.id.imageView);
        executorPlayerImg = (ImageView) findViewById(R.id.executorPlayerImg);
        executorPlayerImg2 = (ImageView) findViewById(R.id.executorPlayerImg2);
        chooseTaskBtn = (Button) findViewById(R.id.chooseTaskBtn);
        taskView = (TextView) findViewById(R.id.taskTxv);
        executorTxv = (TextView) findViewById(R.id.executorTxv);
        gameFrameHolder = (LinearLayout) findViewById(R.id.gameFrame_holder);
        taskComponentsHolder = (LinearLayout) findViewById(R.id.task_components_holder);
        fragmentManager = getFragmentManager();

        saveTaskBtn = (Button) findViewById(R.id.saveTaskBtn);

        chooseTaskBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menu != null){
                    menu.getItem(0).setVisible(false);
                }
                gameFrameHolder.setVisibility(View.VISIBLE);
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                TaskChooserFragment taskChooserFragment = new TaskChooserFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", dataProvider);
                taskChooserFragment.setArguments(bundle);
                transaction.add(R.id.gameFrame_holder, taskChooserFragment);
                transaction.commit();
            }
        });

        saveTaskBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String taskText = taskInput.getText().toString();
                if(!taskText.isEmpty()){
                    taskInput.setText("");
                    checkIfTaskIsNew(taskText);
                    taskComponentsHolder.setVisibility(View.INVISIBLE);
                    executorTxv.setVisibility(View.VISIBLE);
                    ShakeEventManager.start();
                    showNextPlayer(currentPlayerIndex);
                }
            }
        });

        Random random = new Random();
        int startPlayerId = random.nextInt(gameHandler.getPlayers().size());
        currentPlayer = gameHandler.getPlayers().get(startPlayerId);
        gameHandler.getPlayers().get(startPlayerId).setScore(gameHandler.getPlayers().get(startPlayerId).getScore() + 1);
        currentThreeMan = gameHandler.getPlayers().get(startPlayerId);

        if (currentPlayer != null) {
            Picasso.with(getApplicationContext())
                    .load(currentPlayer.getPlayerIcon())
                    .resize(icon_size, icon_size)
                    .centerCrop()
                    .into(currentPlayerImg);
        }
        if (currentThreeMan != null) {
            Picasso.with(getApplicationContext())
                    .load(currentThreeMan.getPlayerIcon())
                    .resize(icon_size, icon_size)
                    .centerCrop()
                    .into(currentThreeManImg);
            gameHandler.setThreeMan(currentThreeMan);
        }

        return startPlayerId;
    }

    @Override
    public void onShake() {
        ShakeEventManager.stop();

        if (shuffleCounter < MAXROUNDS) {
            int diceOne = random.nextInt(6) + 1;
            int diceTwo = random.nextInt(6) + 1;

            switch (diceOne) {
                case 1:
                    dice1Picture.setImageResource(R.drawable.one);
                    break;
                case 2:
                    dice1Picture.setImageResource(R.drawable.two);
                    break;
                case 3:
                    dice1Picture.setImageResource(R.drawable.three);
                    break;
                case 4:
                    dice1Picture.setImageResource(R.drawable.four);
                    break;
                case 5:
                    dice1Picture.setImageResource(R.drawable.five);
                    break;
                case 6:
                    dice1Picture.setImageResource(R.drawable.six);
                    break;
                default:
                    break;
            }

            switch (diceTwo) {
                case 1:
                    dice2Picture.setImageResource(R.drawable.one);
                    break;
                case 2:
                    dice2Picture.setImageResource(R.drawable.two);
                    break;
                case 3:
                    dice2Picture.setImageResource(R.drawable.three);
                    break;
                case 4:
                    dice2Picture.setImageResource(R.drawable.four);
                    break;
                case 5:
                    dice2Picture.setImageResource(R.drawable.five);
                    break;
                case 6:
                    dice2Picture.setImageResource(R.drawable.six);
                    break;
                default:
                    break;
            }

            gameHandler.setDiceEyes1(diceOne);
            gameHandler.setDiceEyes2(diceTwo);

            String output = gameHandler.rollDices(currentPlayer, currentTask, currentThreeManImg, executorPlayerImg, executorPlayerImg2, executorTxv, taskComponentsHolder);
            taskView.setText(output);
            taskView.setVisibility(View.VISIBLE);

            currentPlayerIndex++;
            if (currentPlayerIndex >= gameHandler.getPlayers().size()) {
                currentPlayerIndex = 0;
            }

            if(!gameHandler.isNewTaskToChoose()){
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                if(!shakeForOPtionMenuStop){
                                    ShakeEventManager.start();
                                }
                                showNextPlayer(currentPlayerIndex);
                            }
                        },
                        15000
                );
            } else {
                executorTxv.setVisibility(View.INVISIBLE);
            }
            shuffleCounter++;
        } else {
            taskView.setText(getApplicationContext().getResources().getString(R.string.gameOver));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.game_menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public void onBackPressed() {
        if(gameFrameHolder.getVisibility() == View.VISIBLE){
            if(menu != null){
                menu.getItem(0).setVisible(true);
            }
            if(shakeForOPtionMenuStop){
                ShakeEventManager.start();
                shakeForOPtionMenuStop = false;
            }
            gameFrameHolder.removeAllViews();
            threeManTxv.setVisibility(View.VISIBLE);
            threeManImg.setVisibility(View.VISIBLE);
            activePlayerTxv.setVisibility(View.VISIBLE);
            activePlayerImg.setVisibility(View.VISIBLE);
            dice1.setVisibility(View.VISIBLE);
            dice2.setVisibility(View.VISIBLE);
            taskInput.setVisibility(View.VISIBLE);
            taskView.setVisibility(View.VISIBLE);
            imageView.setVisibility(View.VISIBLE);
            executorPlayerImg.setVisibility(View.VISIBLE);
            executorPlayerImg2.setVisibility(View.VISIBLE);
            gameFrameHolder.setVisibility(View.INVISIBLE);

            String newTask = dataProvider.getNewChosenTask();
            taskInput.setText(newTask);

            if(gameHandler.isNewTaskToChoose()){
                if(newTask != null){
                    checkIfTaskIsNew(newTask);
                }
                taskComponentsHolder.setVisibility(View.VISIBLE);
            } else {
                executorTxv.setVisibility(View.VISIBLE);
            }

        }
        else if(gameFrameHolder.getVisibility() == View.INVISIBLE){
            this.finish();
        }
    }

    private boolean checkIfTaskIsNew(String task){
        boolean unique = true;
        for(String entry: dataProvider.getTasks()){
            if(entry.equals(task)){
                unique = false;
                break;
            }
        }
        if(unique){
            databaseHandler.addTask(task);
            dataProvider.getTasks().add(task);
        }
        return unique;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                item.setVisible(false);
                ShakeEventManager.stop();
                shakeForOPtionMenuStop = true;
                gameFrameHolder.setVisibility(View.VISIBLE);
                statisticFragment = new StatisticFragment();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", dataProvider);
                statisticFragment.setArguments(bundle);
                fragmentTransaction.add(R.id.gameFrame_holder, statisticFragment);
                fragmentTransaction.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}