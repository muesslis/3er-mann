package ch.koller.patrick.a3er_mann.types;

/**
 * Created by admin on 20.09.2017.
 */
public interface GameRule {
    public boolean checkRule(int diceEyes1, int diceEyes2);
    public String getTask();
    public void setTask(String task);
    public int getEyeCount();
    public void setEyeCount(int eyeCount);
}
