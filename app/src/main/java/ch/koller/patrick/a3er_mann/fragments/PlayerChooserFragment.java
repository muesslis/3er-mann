package ch.koller.patrick.a3er_mann.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import ch.koller.patrick.a3er_mann.types.DataProvider;
import ch.koller.patrick.a3er_mann.types.Player;
import ch.koller.patrick.a3er_mann.R;

/**
 * Created by admin on 20.09.2017.
 */
public class PlayerChooserFragment extends Fragment {

    private DataProvider playerProvider;
    private int selectedPlayerIndex;
    private List<Integer> icons;
    private GridView iconHolder;

    private int icon_size;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_player_chooser, container, false);
    }

    private void initComponents() {

        icon_size = getResources().getInteger(R.integer.icon_size_big) * ((int) getResources().getDisplayMetrics().density);

        Bundle bundle = getArguments();
        playerProvider = (DataProvider) bundle.getSerializable("data");
        selectedPlayerIndex = bundle.getInt("selectedPlayerIndex");

        iconHolder = (GridView) getActivity().findViewById(R.id.icon_holder);
        getIcons();

        iconHolder.setAdapter(new GridAdapter(icons));
    }

    private void getIcons() {
        icons = new CopyOnWriteArrayList<>();
        icons.add(R.drawable.giraffe);
        icons.add(R.drawable.gorilla);
        icons.add(R.drawable.loewe);
        icons.add(R.drawable.sloth);
        icons.add(R.drawable.papagei);
        icons.add(R.drawable.pinguin);
        icons.add(R.drawable.murmeltier);
        icons.add(R.drawable.kaiserschnurrbarttamarin);

        for(Integer resource: icons){
            for(Player player:playerProvider.getPlayers()){
                if(resource == player.getPlayerIcon()){
                    icons.remove(resource);
                }
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        initComponents();

    }

    private class GridAdapter extends BaseAdapter {

        private List<Integer> resources;

        GridAdapter(List<Integer> resources) {
            this.resources = resources;
        }

        @Override
        public int getCount() {
            return resources.size();
        }

        @Override
        public Integer getItem(int i) {
            return resources.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ImageButton imageButton = (ImageButton) LayoutInflater.from(getContext()).inflate(R.layout.view_playericon_chooser, null);
            Picasso.with(getActivity().getApplicationContext())
                    .load(resources.get(i))
                    .resize(icon_size, icon_size)
                    .centerCrop()
                    .into(imageButton);
            final int index = i;
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    playerProvider.getPlayers().get(selectedPlayerIndex).setPlayerIcon(icons.get(index));
                    FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    MainFragment mainFragment = new MainFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", playerProvider);
                    mainFragment.setArguments(bundle);
                    fragmentTransaction.replace(R.id.frame_holder, mainFragment);
                    fragmentTransaction.commit();
                }
            });
            return imageButton;
        }
    }

}
