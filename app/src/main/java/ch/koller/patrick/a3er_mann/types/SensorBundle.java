package ch.koller.patrick.a3er_mann.types;

/**
 * Created by Patrick Koller on 25.09.2017.
 */

public class SensorBundle {
    private final float mXAcc;
    private final float mYAcc;
    private final float mZAcc;
    private final long mTimestamp;

    public SensorBundle(float XAcc, float YAcc, float ZAcc, long timestamp) {
        mXAcc = XAcc;
        mYAcc = YAcc;
        mZAcc = ZAcc;
        mTimestamp = timestamp;
    }

    public float getXAcc() {
        return mXAcc;
    }

    public float getYAcc() {
        return mYAcc;
    }

    public float getZAcc() {
        return mZAcc;
    }

    public long getTimestamp() {
        return mTimestamp;
    }

    @Override
    public String toString() {
        return "SensorBundle{" +
                "mXAcc=" + mXAcc +
                ", mYAcc=" + mYAcc +
                ", mZAcc=" + mZAcc +
                ", mTimestamp=" + mTimestamp +
                '}';
    }
}
