package ch.koller.patrick.a3er_mann.handlers;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import java.util.ArrayList;

import ch.koller.patrick.a3er_mann.types.SensorBundle;

/**
 * Created by admin on 20.09.2017.
 */
public class ShakeEventManager implements SensorEventListener {
    private static final float DEFAULT_THRESHOLD_ACCELERATION = 6.0f;
    private static final int DEFAULT_THRESHOLD_SHAKE_NUMBER = 1;
    private static final int INTERVAL = 300;

    private static SensorManager mSensorManager;
    private static ShakeEventManager mSensorEventListener;

    private OnShakeListener mShakeListener;
    private ArrayList<SensorBundle> mSensorBundles;
    private Object mLock;
    private float mThresholdAcceleration;
    private int mThresholdShakeNumber;

    public interface OnShakeListener {
        void onShake();
    }

    public static boolean create(Context context, OnShakeListener listener) {
        if (context == null) {
            throw new IllegalArgumentException("Context must not be null");
        }

        if (mSensorManager == null) {
            mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        }
        mSensorEventListener = new ShakeEventManager(listener);

        return mSensorManager.registerListener(mSensorEventListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
    }

    public static boolean start() {
        if (mSensorManager != null && mSensorEventListener != null) {
            return mSensorManager.registerListener(mSensorEventListener, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_GAME);
        }
        return false;
    }

    public static void stop() {
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(mSensorEventListener);
        }
    }

    public static void destroy() {
        mSensorManager = null;
        mSensorEventListener = null;
    }

    public static void updateConfiguration(float sensibility, int shakeNumber) {
        mSensorEventListener.setConfiguration(sensibility, shakeNumber);
    }

    private ShakeEventManager(OnShakeListener listener) {
        if (listener == null) {
            throw new IllegalArgumentException("Shake listener must not be null");
        }
        mShakeListener = listener;
        mSensorBundles = new ArrayList<SensorBundle>();
        mLock = new Object();
        mThresholdAcceleration = DEFAULT_THRESHOLD_ACCELERATION;
        mThresholdShakeNumber = DEFAULT_THRESHOLD_SHAKE_NUMBER;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        SensorBundle sensorBundle = new SensorBundle(sensorEvent.values[0], sensorEvent.values[1], sensorEvent.values[2], sensorEvent.timestamp);

        synchronized (mLock) {
            if (mSensorBundles.size() == 0) {
                mSensorBundles.add(sensorBundle);
            } else if (sensorBundle.getTimestamp() - mSensorBundles.get(mSensorBundles.size() - 1).getTimestamp() > INTERVAL) {
                mSensorBundles.add(sensorBundle);
            }
        }

        performCheck();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // The accuracy is not likely to change on a real device. Just ignore it.
    }

    private void setConfiguration(float sensibility, int shakeNumber) {
        mThresholdAcceleration = sensibility;
        mThresholdShakeNumber = shakeNumber;
        synchronized (mLock) {
            mSensorBundles.clear();
        }
    }

    private void performCheck() {
        synchronized (mLock) {
            int[] vector = {0, 0, 0};
            int[][] matrix = {
                {0, 0}, // Represents X axis, positive and negative direction.
                {0, 0}, // Represents Y axis, positive and negative direction.
                {0, 0}  // Represents Z axis, positive and negative direction.
            };

            for (SensorBundle sensorBundle : mSensorBundles) {
                if (sensorBundle.getXAcc() > mThresholdAcceleration && vector[0] < 1) {
                    vector[0] = 1;
                    matrix[0][0]++;
                }
                if (sensorBundle.getXAcc() < -mThresholdAcceleration && vector[0] > -1) {
                    vector[0] = -1;
                    matrix[0][1]++;
                }
                if (sensorBundle.getYAcc() > mThresholdAcceleration && vector[1] < 1) {
                    vector[1] = 1;
                    matrix[1][0]++;
                }
                if (sensorBundle.getYAcc() < -mThresholdAcceleration && vector[1] > -1) {
                    vector[1] = -1;
                    matrix[1][1]++;
                }
                if (sensorBundle.getZAcc() > mThresholdAcceleration && vector[2] < 1) {
                    vector[2] = 1;
                    matrix[2][0]++;
                }
                if (sensorBundle.getZAcc() < -mThresholdAcceleration && vector[2] > -1) {
                    vector[2] = -1;
                    matrix[2][1]++;
                }
            }

            for (int[] axis: matrix) {
                for (int direction: axis) {
                    if (direction < mThresholdShakeNumber) {
                        return;
                    }
                }
            }

            mShakeListener.onShake();
            mSensorBundles.clear();
        }
    }
}