package ch.koller.patrick.a3er_mann.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.Locale;

import ch.koller.patrick.a3er_mann.handlers.DatabaseHandler;
import ch.koller.patrick.a3er_mann.GameActivity;
import ch.koller.patrick.a3er_mann.types.Player;
import ch.koller.patrick.a3er_mann.types.DataProvider;
import ch.koller.patrick.a3er_mann.R;

/**
 * Created by admin on 20.09.2017.
 */
public class MainFragment extends Fragment {

    private LinearLayout playerSelectionHolderStatic;
    private LinearLayout playSelectionHolderDynamic;
    private Button chooseTaskBtn;
    private Button addPlayerBtn;
    private Button startGameBtn;
    private EditText taskInput;
    private DataProvider dataProvider;
    private DatabaseHandler databaseHandler;

    private int icon_size;

    private FragmentManager fragmentManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        initComponents();
    }

    private void initComponents() {
        databaseHandler = new DatabaseHandler(getActivity());
        icon_size = getResources().getInteger(R.integer.icon_size_small) * ((int) getResources().getDisplayMetrics().density);

        playerSelectionHolderStatic = (LinearLayout) getActivity().findViewById(R.id.player_selection_holder);
        chooseTaskBtn = (Button) getActivity().findViewById(R.id.chooseTaskBtn);
        addPlayerBtn = (Button) getActivity().findViewById(R.id.addPlayerBtn);
        startGameBtn = (Button) getActivity().findViewById(R.id.start_game_btn);
        taskInput = (EditText) getActivity().findViewById(R.id.taskInput);
        playSelectionHolderDynamic = (LinearLayout) getActivity().findViewById(R.id.player_selection_holder_dynamic);

        Bundle bundle = getArguments();
        dataProvider = (DataProvider) bundle.getSerializable("data");
        taskInput.setText(dataProvider.getTask() != null ? dataProvider.getTask(): "");
        fragmentManager = getFragmentManager();

        updatePlayerSelectionView();

        chooseTaskBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                TaskChooserFragment taskChooserFragment = new TaskChooserFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", dataProvider);
                taskChooserFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.frame_holder, taskChooserFragment).addToBackStack("taskChooser");
                fragmentTransaction.commit();
            }
        });

        addPlayerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataProvider.getPlayers().size() < 8) {
                    dataProvider.getPlayers().add(new Player(0, 0));
                    updatePlayerSelectionView();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "8 Spieler ist das Limit", Toast.LENGTH_SHORT).show();
                }

            }
        });

        startGameBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean playerWithoutIcon = false;
                LinkedList<Player> players = dataProvider.getPlayers();
                for (Player player : players) {
                    if (player.getPlayerIcon() == 0) {
                        playerWithoutIcon = true;
                        break;
                    }
                }
                String task = taskInput.getText().toString();
                if (!playerWithoutIcon) {
                    if (!task.isEmpty()) {
                        checkIfTaskIsNew(task);
                        dataProvider.setTask(task);
                        Intent gameActivityIntent = new Intent(getActivity(), GameActivity.class);
                        gameActivityIntent.putExtra("data", dataProvider);
                        startActivity(gameActivityIntent);
                    } else {
                        Toast.makeText(getActivity().getApplicationContext(), "Eine Startaufgabe muss noch eingegeben werden", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), "Einige Spieler benötigen noch Bilder", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private boolean checkIfTaskIsNew(String task){
        boolean unique = true;
        for(String entry: dataProvider.getTasks()){
            if(entry.equals(task)){
                unique = false;
                break;
            }
        }
        if(unique){
            databaseHandler.addTask(task);
            dataProvider.getTasks().add(task);
        }
        return unique;
    }

    private void updatePlayerSelectionView() {
        playerSelectionHolderStatic.removeAllViews();
        playSelectionHolderDynamic.removeAllViews();
        int count = 0;
        for (Player player : dataProvider.getPlayers()) {
            if (count < 4) {
                playerSelectionHolderStatic.addView(createPlayerSelectionView(count));
            } else {
                playSelectionHolderDynamic.addView(createPlayerSelectionView(count));
            }
            count++;
        }
    }

    private LinearLayout createPlayerSelectionView(final int playerIndex) {
        LinearLayout holder = new LinearLayout(getActivity().getApplicationContext());
        holder.setOrientation(LinearLayout.HORIZONTAL);
        holder.setLayoutParams(new LinearLayoutCompat.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        holder.setGravity(Gravity.CENTER);
        TextView text = new TextView(getActivity().getApplicationContext());
        text.setTextSize(getResources().getInteger(R.integer.text_size_small) * getResources().getDisplayMetrics().density);
        text.setText(String.format(Locale.getDefault(), "%s %d", getString(R.string.player), playerIndex + 1));
        text.setTextColor(Color.BLACK);
        ImageButton playerIcon = new ImageButton(getActivity().getApplicationContext());
        playerIcon.setBackgroundColor(getResources().getColor(R.color.defaultBackgroundColor));
        if (dataProvider.getPlayers().get(playerIndex).getPlayerIcon() != 0) {
            Picasso.with(getActivity().getApplicationContext())
                    .load(dataProvider.getPlayers().get(playerIndex).getPlayerIcon())
                    .resize(icon_size, icon_size)
                    .centerCrop()
                    .into(playerIcon);
        } else {
            Picasso.with(getActivity().getApplicationContext())
                    .load(R.drawable.ic_add_circle_outline_black_24dp)
                    .resize(icon_size, icon_size)
                    .centerCrop()
                    .into(playerIcon);
        }
        playerIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                choosePlayerImage(playerIndex);
            }
        });
        TextView spacer = new TextView(getActivity().getApplicationContext());
        spacer.setWidth(20);
        holder.addView(text);
        holder.addView(spacer);
        holder.addView(playerIcon);
        return holder;
    }

    private void choosePlayerImage(int playerIndex) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        PlayerChooserFragment playerChooserFragment = new PlayerChooserFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", dataProvider);
        bundle.putInt("selectedPlayerIndex", playerIndex);
        playerChooserFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.frame_holder, playerChooserFragment).addToBackStack("playerChooseFragment");
        fragmentTransaction.commit();
    }
}
