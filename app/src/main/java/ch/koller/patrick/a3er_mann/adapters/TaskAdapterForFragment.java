package ch.koller.patrick.a3er_mann.adapters;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ch.koller.patrick.a3er_mann.types.DataProvider;
import ch.koller.patrick.a3er_mann.R;
import ch.koller.patrick.a3er_mann.fragments.MainFragment;

/**
 * Created by admin on 25.09.2017.
 */
public class TaskAdapterForFragment extends BaseAdapter {

    private DataProvider dataProvider;
    private List<String> tasks;
    private Activity activity;

    public TaskAdapterForFragment(List<String> tasks, DataProvider dataProvider ,Activity activity) {
        this.tasks = tasks;
        this.dataProvider = dataProvider;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public String getItem(int position) {
        return tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
}

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        TextView listEntry = ((TextView) LayoutInflater.from(activity).inflate(R.layout.view_task_chooser, null));
        listEntry.setTextSize(8 * activity.getResources().getDisplayMetrics().density);
        listEntry.setText(getItem(position));
        listEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataProvider.setTask(getItem(position));
                FragmentManager fm = activity.getFragmentManager();
                MainFragment mainFragment = new MainFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", dataProvider);
                mainFragment.setArguments(bundle);
                fm.beginTransaction().replace(R.id.frame_holder, mainFragment).commit();
            }
        });
        return listEntry;
    }
}
