package ch.koller.patrick.a3er_mann.types;

import ch.koller.patrick.a3er_mann.types.GameRule;

/**
 * Created by admin on 20.09.2017.
 */
public class IndivRule implements GameRule {
    private String task;
    private int eyeCount;

    public IndivRule(String task, int eyeCount) {
        this.task = task;
        this.eyeCount = eyeCount;
    }

    @Override
    public boolean checkRule(int diceEyes1, int diceEyes2) {
        return false;
    }

    @Override
    public String getTask() {
        return task;
    }

    @Override
    public void setTask(String task) {
        this.task = task;
    }

    @Override
    public int getEyeCount() {
        return eyeCount;
    }

    @Override
    public void setEyeCount(int eyeCount) {
        this.eyeCount = eyeCount;
    }
}
