package ch.koller.patrick.a3er_mann;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import java.util.List;

import ch.koller.patrick.a3er_mann.fragments.MainFragment;
import ch.koller.patrick.a3er_mann.handlers.DatabaseHandler;
import ch.koller.patrick.a3er_mann.types.DataProvider;
import ch.koller.patrick.a3er_mann.types.Player;

public class MainActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;
    private MainFragment mainFragment;
    private DatabaseHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new DatabaseHandler(this);
        /*
        db.addTask("Um das Haus rennen.");
        db.addTask("Katze streicheln.");
        */

        setContentView(R.layout.activity_main);
        fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        mainFragment = new MainFragment();

        List<String> tasks = db.getAllTasks();
        DataProvider dataProvider = getDataProvider();
        dataProvider.setTasks(tasks);

        Bundle bundle = new Bundle();
        bundle.putSerializable("data", dataProvider);
        mainFragment.setArguments(bundle);
        fragmentTransaction.add(R.id.frame_holder, mainFragment);
        fragmentTransaction.commit();
    }

    private DataProvider getDataProvider() {
        DataProvider dataProvider = new DataProvider();
        for(int i = 1; i<=4; i++){
            dataProvider.addPlayer(new Player(0, 0));
        }
        return dataProvider;
    }

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 0){
            getFragmentManager().popBackStack();
        } else{
            super.onBackPressed();
        }
    }
}
