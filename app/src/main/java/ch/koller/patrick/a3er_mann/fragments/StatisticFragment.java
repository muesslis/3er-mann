package ch.koller.patrick.a3er_mann.fragments;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import ch.koller.patrick.a3er_mann.types.DataProvider;
import ch.koller.patrick.a3er_mann.types.Player;
import ch.koller.patrick.a3er_mann.R;

/**
 * Created by admin on 25.09.2017.
 */
public class StatisticFragment extends Fragment {
    private LinearLayout playerColumnLinearLayout;
    private LinkedList<Player> players;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_statistic, container, false);
    }

    @Override
    public void onStart(){
        super.onStart();

        playerColumnLinearLayout = (LinearLayout) getActivity().findViewById(R.id.playerColumnLinearLayout);

        DataProvider dataProvider = (DataProvider) getArguments().get("data");
        players = dataProvider.getPlayers();

        int imageSize = 170;
        int margin = 10;

        if(players.size() < 5){
            imageSize = 256;
            margin = 50;
        }
        else if(players.size() < 7){
            imageSize = 200;
            margin = 30;
        }

        Collections.sort(players, new Comparator<Player>() {
            @Override
            public int compare(Player p1, Player p2) {
                if(p1.getScore() < p2.getScore()){
                    return 1;
                }
                else if(p1.getScore() > p2.getScore()){
                    return -1;
                }
                else{
                    return 0;
                }
            }
        });

        playerColumnLinearLayout.removeAllViews();

        for (Player player : players){
            LinearLayout horizontalLayout = new LinearLayout(getActivity());
            horizontalLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);

            ImageView playerIcon = new ImageView(getActivity());
            Picasso.with(getActivity().getApplicationContext())
                    .load(player.getPlayerIcon())
                    .resize(imageSize, imageSize)
                    .centerCrop()
                    .into(playerIcon);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(250, margin, 250, margin);

            TextView score = new TextView(getActivity());
            score.setTextColor(Color.parseColor("#000000"));
            score.setTextSize(22);
            score.setText(String.valueOf(player.getScore()));
            score.setLayoutParams(params);

            horizontalLayout.addView(playerIcon);
            horizontalLayout.addView(score);

            playerColumnLinearLayout.addView(horizontalLayout);
        }

        LinearLayout gameFrameHolder = (LinearLayout) getActivity().findViewById(R.id.gameFrame_holder);
        gameFrameHolder.setVisibility(View.VISIBLE);

        hideEverything();
    }

    private void hideEverything(){
        TextView threeManTxv = (TextView) getActivity().findViewById(R.id.threeManTxv);
        ImageView threeManImg = (ImageView) getActivity().findViewById(R.id.threeManImg);
        TextView activePlayerTxv = (TextView) getActivity().findViewById(R.id.activePlayerTxv);
        ImageView activePlayerImg = (ImageView) getActivity().findViewById(R.id.activePlayerImg);
        ImageView dice1 = (ImageView) getActivity().findViewById(R.id.dice1);
        ImageView dice2 = (ImageView) getActivity().findViewById(R.id.dice2);
        TextView taskTxv = (TextView) getActivity().findViewById(R.id.taskTxv);
        TextView executorTxv = (TextView) getActivity().findViewById(R.id.executorTxv);
        LinearLayout taskComponentsHolder = (LinearLayout)getActivity().findViewById(R.id.task_components_holder);
        ImageView imageView = (ImageView) getActivity().findViewById(R.id.imageView);
        ImageView executorPlayerImg2 = (ImageView) getActivity().findViewById(R.id.executorPlayerImg2);
        ImageView executorPlayerImg = (ImageView) getActivity().findViewById(R.id.executorPlayerImg);

        threeManTxv.setVisibility(View.INVISIBLE);
        threeManImg.setVisibility(View.INVISIBLE);
        activePlayerTxv.setVisibility(View.INVISIBLE);
        activePlayerImg.setVisibility(View.INVISIBLE);
        dice1.setVisibility(View.INVISIBLE);
        dice2.setVisibility(View.INVISIBLE);
        taskTxv.setVisibility(View.INVISIBLE);
        executorTxv.setVisibility(View.INVISIBLE);
        taskComponentsHolder.setVisibility(View.INVISIBLE);
        imageView.setVisibility(View.INVISIBLE);
        executorPlayerImg.setVisibility(View.INVISIBLE);
        executorPlayerImg2.setVisibility(View.INVISIBLE);
    }
}
