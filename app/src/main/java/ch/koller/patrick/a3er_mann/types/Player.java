package ch.koller.patrick.a3er_mann.types;

import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by admin on 20.09.2017.
 */
public class Player implements Serializable{
    private int playerIcon;
    private int score;

    public Player(int playerIcon, int score) {
        this.playerIcon = playerIcon;
        this.score = score;
    }

    public int getPlayerIcon() {
        return playerIcon;
    }

    public void setPlayerIcon(int playerIcon) {
        this.playerIcon = playerIcon;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}